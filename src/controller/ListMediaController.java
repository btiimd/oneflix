package controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class ListMediaController {

	private MainApp mainApp;

	@FXML
	ImageView iv00;

	@FXML
	ImageView iv01;

	@FXML
	ImageView iv02;

	@FXML
	ImageView iv10;

	@FXML
	ImageView iv11;

	@FXML
	ImageView iv12;

	@FXML
	FXMLLoader loader;

	@FXML
	AnchorPane detailMedia; 

	public void setMainApp(MainApp mainApp){
		this.mainApp = mainApp;
	}

	@FXML
	private void initialize(){

		Image image = new Image("/images/cartaz.jpg");

		iv00.setImage(image);
		iv01.setImage(image);
		iv02.setImage(image);
		iv10.setImage(image);
		iv11.setImage(image);
		iv12.setImage(image);

		iv00.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {

				try {
					
					loader = new FXMLLoader();
					loader.setLocation(MainApp.class.getResource("../view/DetailMedia.fxml"));
					detailMedia = (AnchorPane) loader.load();
					
					DetailMediaController detailMediaController = loader.getController();
					detailMediaController.setMainApp(mainApp);
					
					mainApp.getRootBorderLayout().setCenter(detailMedia);
					
				} catch (Exception e) {

				}
				
				event.consume();
			}
		});

	}

}
