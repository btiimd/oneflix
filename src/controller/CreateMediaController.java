package controller;

import java.io.File;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class CreateMediaController {

	private MainApp mainApp;

	@FXML
	TextField tfName;

	@FXML
	TextArea tfDescription;

	@FXML
	TextField tfYear;

	@FXML
	TextField tfSeason;

	@FXML
	TextField tfEpisode;

	@FXML
	TextField tfDuration;

	@FXML
	TextField tfCategory;

	@FXML
	TextField tfDirector;

	@FXML
	TextField tfMainActor;

	@FXML
	TextField tfAgeGroup;
	
	@FXML
	Label lbMedia;
	
	@FXML
	Label lbMediaImage;

	public void setMainApp(MainApp mainApp){
		this.mainApp = mainApp;
	}

	@FXML
	public void handlerMedia(){

		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Midia", "*.avi", "*.mp4"));
		
		File selectedFile = fileChooser.showOpenDialog(mainApp.getPrimaryStage());
		
		if (selectedFile != null) {
			lbMedia.setText( selectedFile.getName() );
		}

	}
	
	@FXML
	public void handlerMediaImage(){
		
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Imagens", "*.jpg", "*.png"));
		
		File selectedFile = fileChooser.showOpenDialog(mainApp.getPrimaryStage());
		
		if (selectedFile != null) {
			lbMediaImage.setText( selectedFile.getName() );
		}
		
	}

	@FXML
	public void handlerSave(){

	}

}
