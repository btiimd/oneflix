package controller;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class DetailMediaController {

	private MainApp mainApp;
	
	@FXML
	ImageView ivMedia;

	@FXML
	Label lbName;
	
	@FXML
	Label lbDescription;
	
	@FXML
	Label lbYear;
	
	@FXML
	Label lbSeason;
	
	@FXML
	Label lbEpisode;
	
	@FXML
	Label lbCategory;
	
	@FXML
	Label lbDirector;
	
	@FXML
	Label lbMainActor;
	
	@FXML
	Label lbAgeGroup;
	
	@FXML
	FXMLLoader loader;
	
	@FXML
	AnchorPane showMedia;
	
	@FXML
	Button btFavorite;
	
	public void setMainApp(MainApp mainApp){
		
		this.mainApp = mainApp;
		
	}
	
	@FXML
	public void handlerShow(){
		
		// Carrega o sign up.
		loader = new FXMLLoader();
		loader.setLocation(MainApp.class.getResource("../view/ShowMedia.fxml"));
		
		try {
			showMedia = (AnchorPane) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ShowMediaController showMediaController = loader.getController();
		showMediaController.setMainApp(mainApp);
		
		mainApp.getRootBorderLayout().setCenter(showMedia);
		
	}
	
	@FXML
	public void handlerFavorite(){
		
		btFavorite.setText("Favoritado!");
		
	}
	
	
}
