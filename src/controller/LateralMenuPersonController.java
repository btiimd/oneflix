package controller;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class LateralMenuPersonController {

	private MainApp mainApp;
	
	@FXML
	FXMLLoader loader;
	
	@FXML
	AnchorPane searchMedia;
	
	@FXML
	AnchorPane listMedia;
	
	public void setMainApp(MainApp mainApp){
		this.mainApp = mainApp;
	}
	
	@FXML
	public void handlerHome(){
		
		try {
			
			// Carrega o sign up.
			loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("../view/ListMedia.fxml"));
			listMedia = (AnchorPane) loader.load();
			
			ListMediaController listMediaController = loader.getController();
			listMediaController.setMainApp(mainApp);
		
			mainApp.getRootBorderLayout().setCenter(listMedia);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@FXML
	public void handlerSearchMedia(){
		
		// Carrega o sign up.
		loader = new FXMLLoader();
		loader.setLocation(MainApp.class.getResource("../view/SearchMedia.fxml"));
		
		try {
			searchMedia = (AnchorPane) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		SearchMediaController searchMediaController = loader.getController();
		searchMediaController.setMainApp(mainApp);
	
		mainApp.getRootBorderLayout().setCenter(searchMedia);
		
	}
	
	@FXML
	AnchorPane signIn;
	
	@FXML
	AnchorPane lateralMenu;
	
	@FXML
	public void handlerLogout(){
		
		// Carrega o sign up.
		loader = new FXMLLoader();
		loader.setLocation(MainApp.class.getResource("../view/SignIn.fxml"));
		
		try {
			signIn = (AnchorPane) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		SignInController signInController = loader.getController();
		signInController.setMainApp(mainApp);
		
		// Carrega o sign up.
		loader = new FXMLLoader();
		loader.setLocation(MainApp.class.getResource("../view/LateralMenu.fxml"));
		
		try {
			lateralMenu = (AnchorPane) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LateralMenuController lateralMenuController = loader.getController();
		lateralMenuController.setMainApp(mainApp);
	
		mainApp.getRootBorderLayout().setCenter(signIn);
		mainApp.getRootBorderLayout().setLeft(lateralMenu);
		
	}
	
	
}
