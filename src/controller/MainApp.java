package controller;

import java.io.IOException;
import java.sql.SQLException;
<<<<<<< HEAD
import java.util.ArrayList;
import java.util.List;
=======
>>>>>>> c863b877127917546c1aa7d8ae3fa42de2eea13f

import com.mysql.jdbc.Connection;

import domain.Database;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Media;

public class MainApp extends Application {

<<<<<<< HEAD
	private Stage primaryStage;
	
	@FXML
	private BorderPane rootBorderLayout;

	Scene scene;
	FXMLLoader loader;
	
	@FXML
	AnchorPane lateralMenu;
	
	@FXML
	AnchorPane signIn;
	
	@FXML
	AnchorPane signUp;

	@FXML
	public void initialize(){
		
	
	}
	
	@Override
	public void start(Stage primaryStage) {

		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("OneFlix");

		initRootLayout();

	}

	/**
	 * Inicializa o root layout (layout base).
	 */
	public void initRootLayout() {

		try {
			
			// Carrega o root layout do arquivo fxml.
			loader = new FXMLLoader();
			loader.setController(this);
			loader.setLocation(MainApp.class.getResource("../view/Login.fxml"));
			rootBorderLayout = (BorderPane) loader.load();
			
			// Carrega o sign up.
			loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("../view/SignUp.fxml"));
			signUp = (AnchorPane) loader.load();
			
			// Carrega o sign up.
			loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("../view/LateralMenu.fxml"));
			lateralMenu = (AnchorPane) loader.load();
			
			LateralMenuController lateralMenuController = loader.getController();
			lateralMenuController.setMainApp(this);

			rootBorderLayout.setCenter(signUp);
			rootBorderLayout.setLeft(lateralMenu);
			
			// Mostra a scene (cena) contendo o root layout.
			scene = new Scene(rootBorderLayout);
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	

	/**
	 * Retorna o palco principal.
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main(String[] args) {
		launch(args);
	}

	public BorderPane getRootBorderLayout() {
		return rootBorderLayout;
	}

	public void setRootBorderLayout(BorderPane rootBorderLayout) {
		this.rootBorderLayout = rootBorderLayout;
	}

	
=======
    private Stage primaryStage;
    private BorderPane rootBorderLayout;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("OneFlix");

        initRootLayout();

        showSignIn();
    }

    /**
     * Inicializa o root layout (layout base).
     */
    public void initRootLayout() {
        
    	try {
            // Carrega o root layout do arquivo fxml.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/Login.fxml"));
            rootBorderLayout = (BorderPane) loader.load();

            // Mostra a scene (cena) contendo o root layout.
            Scene scene = new Scene(rootBorderLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    	
    }

    /**
     * Mostra o person overview dentro do root layout.
     */
    public void showSignIn() {
        try {
            // Carrega o person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/SignIn.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();

            // Define o person overview dentro do root layout.
            rootBorderLayout.setCenter(personOverview);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retorna o palco principal.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        launch(args);
    }
>>>>>>> c863b877127917546c1aa7d8ae3fa42de2eea13f
}
