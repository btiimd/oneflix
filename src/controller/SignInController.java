package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class SignInController {
	
	@FXML
	private TextField tlLoginSignIn;
	
	@FXML
	private PasswordField tlPasswordSignIn;
	
	@FXML
	AnchorPane lateralMenuPerson;
	
	@FXML
	AnchorPane lateralMenuAdmin;
	
	@FXML
	AnchorPane listMedia;
	
	FXMLLoader loader;
	
	//Reference to the main application.
	private MainApp mainApp;
	
	public SignInController(){
		
	}
	
	 /**
     * � chamado pela aplica��o principal para dar uma refer�ncia de volta a si mesmo.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    
    
    @FXML
   	private void handlerLogar(){
    	
   		try {

			loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("../view/ListMedia.fxml"));
			listMedia = (AnchorPane) loader.load();
			
			ListMediaController listMediaController = loader.getController();
			listMediaController.setMainApp(mainApp);
			
			boolean conta = false;
			
			if(conta){
				
				loader = new FXMLLoader();
				loader.setLocation(MainApp.class.getResource("../view/LateralMenuPerson.fxml"));
				lateralMenuPerson = (AnchorPane) loader.load();
				
				LateralMenuPersonController lateralMenuPersonController = loader.getController();
				lateralMenuPersonController.setMainApp(mainApp);

				mainApp.getRootBorderLayout().setLeft(lateralMenuPerson);
				
			} else {
				
				loader = new FXMLLoader();
				loader.setLocation(MainApp.class.getResource("../view/LateralMenuAdmin.fxml"));
				lateralMenuAdmin = (AnchorPane) loader.load();
				
				LateralMenuAdminController lateralMenuAdminController = loader.getController();
				lateralMenuAdminController.setMainApp(mainApp);
				

				mainApp.getRootBorderLayout().setLeft(lateralMenuAdmin);
				
			}
			
	   			
			mainApp.getRootBorderLayout().setCenter(listMedia);
		
        } catch(Exception e) {
        	e.printStackTrace();
        }
   		
   	}
   
}
