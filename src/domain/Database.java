package domain;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class Database {

    private static final String USER = "root";
    private static final String PASSWORD = "";
    private static final String URL = "jdbc:mysql://localhost/oneflixdb";
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    
    public Connection open()  {
    	
        try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println("Erro class for name");
			e.printStackTrace();
		}
        
        Connection connection = null;
        
		try {
			connection = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException e) {
			System.out.println("Erro na conex�o com o banco");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return connection;
    }
    
    public ResultSet query(String query, Connection connection) {
        
        PreparedStatement stmt = null;
        
		try {
			stmt = (PreparedStatement) connection.prepareStatement(query);
		} catch (SQLException e) {
			System.out.println("Erro statement");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        ResultSet result = null;
        
        try {
        	
			stmt.setString(1, "login");
			stmt.setString(2, "password");
			stmt.setString(3, "category");
			
		} catch (SQLException e1) {
			System.out.println("Erro stmt");
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
		try {
			result = stmt.executeQuery();
		} catch (SQLException e) {
			System.out.println("Erro na execu��o da query");
			e.printStackTrace();
		}
		
		try {
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Erro ao fechar stmt");
			e.printStackTrace();
		}
		
        return result;
    } 
    
    public void close(Connection connection) {
    	try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
}